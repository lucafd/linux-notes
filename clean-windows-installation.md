# Clean Windows Installation with partition management

The main goal is to install Windows from USB (assumed you have a bootable USB) with a larger EFI 
partition.

1. Select in the installation interface "Custom Installation";
2. delete any partition present that you see;
3. UNDERSTAND WHICH PARTITION HOLDS THE UEFI-BIOS;
4. make sure you have a clean empty installation media (how to delete everything);
5. click "New" and then "APPLY" (then delete `System`, `MsR`, `Primary`);
6. press the keys `SHIFT`+`F10` to enter into the command line interface;
7. type `diskpart.exe` and press `ENTER` to list your available disks;
8. type`select disk N` where `N` is the number for the disk you want to install to as identified by the above command and press `ENTER`;
9. type `create partition efi size =550` where `550` is the desired size of the ESP in Mebibytes (MiB) and press `ENTER`;
10. type `format quick fs=fat32 label=System` and press `ENTER` to format the partition;
11. type `exit` and `ENTER` to exit the disk partition tool;
12. type `exit` again (and `ENTER`) to exit the command line;
13. graphical Windows setup, press "Refresh" to see the present state of the partitions;
14. select unallocated space and click "New"

This are some commands that I don't know why they where there.
```
list partition
clean
list partition
convert gpt
```
The second `list` is to ensure all partitions have been removed, the `convert gpt` is needed only if list disk had no asterisk (?).
