# 30 Things to Do After Installing Debian 10 or DEBIAN 11

**Debian is a great desktop distro but you  need to spend quite some time to configure it for the best desktop  experience possible. Luckily, I have collected all the possible things  you can do after installing Debian.**

**Everything here descripted works with debian 11 installation!**

## Content

- [x] [Fix CD-ROM error](https://averagelinuxuser.com/debian-10-after-install/#1-fix-cd-rom-error)
- [ ] [Switch to the fastest repository mirror](https://averagelinuxuser.com/debian-10-after-install/#2-switch-to-the-fastest-repository-mirror)
- [x] [Add contrib and non-free repositories](https://averagelinuxuser.com/debian-10-after-install/#3-add-contrib-and-non-free-repositories)
- [x] [Switch to Xorg](https://averagelinuxuser.com/debian-10-after-install/#4-switch-to-xorg)
- [x] [Return minimize button](https://averagelinuxuser.com/debian-10-after-install/#5-return-minimize-button)
- [x] [Install Synaptic](https://averagelinuxuser.com/debian-10-after-install/#6-install-synaptic)
- [x] [Install microcode](https://averagelinuxuser.com/debian-10-after-install/#7-install-microcode)
- [x] [Install build-essential](https://averagelinuxuser.com/debian-10-after-install/#8-install-build-essential)
- [x] [Install Drivers](https://averagelinuxuser.com/debian-10-after-install/#9-install-drivers)
- [x] [Install Restricted-extras packages](https://averagelinuxuser.com/debian-10-after-install/#10-install-restricted-extras-packages)
- [x] [Install VLC](https://averagelinuxuser.com/debian-10-after-install/#11-install-vlc)
- [ ] [Install and configure Firewall](https://averagelinuxuser.com/debian-10-after-install/#12-install-and-configure-firewall)
- [ ] [Install backup program](https://averagelinuxuser.com/debian-10-after-install/#13-install-backup-program)
- [ ] [Configure Swappiness](https://averagelinuxuser.com/debian-10-after-install/#14-configure-swappiness)
- [ ] [Speed up the boot time](https://averagelinuxuser.com/debian-10-after-install/#15-speed-up-the-boot-time)
- [ ] [Enable Drive Cache](https://averagelinuxuser.com/debian-10-after-install/#16-enable-drive-cache)
- [ ] [Xkill shortcut](https://averagelinuxuser.com/debian-10-after-install/#17-xkill-shortcut)
- [x] [Ctrl+Alt+T to open Terminal](https://averagelinuxuser.com/debian-10-after-install/#18-ctrlaltt-to-open-terminal)
- [x] [Enable GNOME extensions](https://averagelinuxuser.com/debian-10-after-install/#19-enable-gnome-extensions)
- [x] [Install Desktop icons extension](https://averagelinuxuser.com/debian-10-after-install/#20-install-desktop-icons-extension)
- [x] [Install additional themes](https://averagelinuxuser.com/debian-10-after-install/#21-install-additional-themes)
- [ ] [Add user image](https://averagelinuxuser.com/debian-10-after-install/#22-add-user-image)
- [x] [Change LibreOffice look](https://averagelinuxuser.com/debian-10-after-install/#23-change-libreoffice-look)
- [ ] [Add Files bookmarks](https://averagelinuxuser.com/debian-10-after-install/#24-add-files-bookmarks)
- [ ] [Enable Tray icons](https://averagelinuxuser.com/debian-10-after-install/#25-enable-tray-icons)
- [ ] [Enable Night Light](https://averagelinuxuser.com/debian-10-after-install/#26-enable-night-light)
- [ ] [Firefox settings](https://averagelinuxuser.com/debian-10-after-install/#27-firefox-settings)
- [ ] [Enable Snap and FlatPak Support](https://averagelinuxuser.com/debian-10-after-install/#28-enable-snap-and-flatpak-support)
- [ ] [Extend the battery life for Laptops](https://averagelinuxuser.com/debian-10-after-install/#29-extend-the-battery-life-for-laptops)
- [ ] [Remove unnecessary apps](https://averagelinuxuser.com/debian-10-after-install/#30-remove-unnecessary-apps)

## Video

<iframe src="https://www.youtube.com/embed/y7pETJpOQhg" allowfullscreen="" width="720" height="480">
  </iframe>

​    

## 1. Fix CD-ROM error

This fix is needed only for those **who installed Debian from a DVD**. Because after you installed Debian from a DVD, you will get the error *“the repository cdrom does not have a release file”* every time you try to update your system:

![Debian 10 ERROR: the repository cdrom does not have a release file](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/011-debian10-error-cdrom.jpeg)

To fix this error, open the **Software and Updates** application and in the **Other Software** tab, **disable CDROM repository**:

![disable CDROM repository in Debian 10](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/012-disable-cdrom-repo.jpeg)

Now, the CDROM error is fixed and you should be able to refresh the repositories and update your system without problems.

```
sudo apt update
sudo apt upgrade
```

## 2. Switch to the fastest repository mirror

You can switch to the fastest repository mirror to download the  updates from a server that is physically closer to your location. To  that end, open the **Software and Updates** application → **Debian Software** → **Download from** → **Other** → **Selected the best server**:

![Looking for the fastest repository mirror in Debian 10](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/021-fastest-repo.jpeg)

And Debian will automatically find the fastest server in your location. Select it and **close without reloading** the repositories.

Now, update the repositories through the terminal:

```
sudo apt update
```

If you **see the error**: *The repository XXX doesn’t have a Release file*:

![Debian 10 ERROR: The repository XXX doesn't have a Release file](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/022-error-repo-no-release-file.jpeg)

It means your local mirror doesn’t have a security repository. To fix this, open the sources list:

```
sudo nano /etc/apt/sources.list
```

And replace the address in the lines containing `buster/updates` with the **default security repository**: `http://security.debian.org/debian-security/`:

![Returning the default security repository of Debian 10](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/023-security-repo-debian10.gif)

Press *Ctrl+O* and *Enter* to save the changes and *Ctrl+O* and *Enter* to exit *Nano*. Update the repositories again to make sure the error has disappeared:

```
sudo apt update
```

If everything works fine, you will now receive all the updates from the **local mirror** and the security updates from the **main server**.

## 3. Add contrib and non-free repositories

**Contrib** and **non-free** repositories include many useful packages that are not available in the Debian default repositories. For example, *Dropbox*, some codes, *Nvidia* drivers and many others. To add contrib and non-free repositories, activate them in **Software and Updates** application:

![Adding contrib and non-free repositories in Debian 10](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/031-contrib-non-free-debian10.jpeg)

And reload the information about the available repositories:

![Reload the repositories information](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/032-reload-repositories.jpeg)

Now, you will have access to almost all popular programs on Linux. Just search for them on the **Software Center**.

## 4. Switch to Xorg

**Wayland** is the default display server in Debian. It  has many performance benefits but it is still relatively new and some  apps do not work in Wayland. For example, in my [Debian 10 review](https://averagelinuxuser.com/debian-10-gnome/), I showed that **Synaptic, SimpleScreenRecorder, and Color picker do not work with Wayland**. I need these apps and to make them work I switched to Xorg by selecting System X11 default in the login screen:

![Selecting Xorg in the login screen of Debian 10](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/041-xorg-debian10.jpeg)

To find out what display server you are running, use this command:

```
ps -e | grep tty
```

![Linux command to find out what display server you are running](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/042-display-server-command.jpeg)

## 5. Return minimize button

I like minimalism but only until it doesn’t compromise the functionality and comfort. Removing the minimize button does affect my comfort. So, to return it back.

To that end, I **open Tweaks**, luckily unlike Ubuntu Debian 10 has Tweaks installed by default, and in the **Windows Titlebars**, enable it:

![Returning minimize button in Debian 10](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/051-minimize-button-debian10.jpeg)

You can also enable the maximize button there, but I prefer to use double click on the titlebar to  maximize my windows.

## 6. Install Synaptic

**Synaptic** is an old school graphical package manager but it’s **proven by time**. It is extremely reliable and very powerful. Using Synaptic, you can  find and install many libraries and packages that are not visible in the Software Center. For example, you will [find microcode packages in Synaptic](https://averagelinuxuser.com/debian-10-after-install/#7-install-microcode), but not in the  Software Center. Of course, Synaptic is **not as pretty** as the Software Center but it is **user-friendlier** than the command line.

![Synaptic in Debian 10](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/061-synaptic-debian10.jpeg)

You can find and install Synaptic through the **Software Center** or run:

```
sudo apt install synaptic
```

## 7. Install microcode

**Microcode** is a CPU **firmware** that controls how a processor works. It is better to have the latest microcode for **stability and security** of the system. So, by installing microcode you ensure you receive the microcode updates.

This is where Synaptic becomes handy. Search for microcode in  Synaptic and install either Intel or AMD microcode depending on the type of your processor:

![Intel and AMD microcode in Synaptic](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/071-microcode-synaptic.jpeg)

## 8. Install build-essential

At this step, I recommend installing the packages essential for  compilation and installation of some programs. It is better to install  them early and forget. You can install them with this command:

```
sudo apt install build-essential dkms linux-headers-$(uname -r)
```

## 9. Install Drivers

Likely, your Debian works fine with the opensource drivers (nouveau  for Nvidia, and amdgpu for AMD). If it is the case, you probably even do not need to install proprietary drivers. However, if you experience  some graphical problems, installing proprietary drivers, may fix these  problems.

### Nvidia driver

If you have an **Nvidia card**, you first need to check what driver is required for your system. To do that, install nvidia-detect:

```
sudo apt install nvidia-detect
```

And run it:

```
sudo nvidia-detect
```

Most likely, you will see that you need the nvidia-driver package:

![nvidia-detect suggests to install nvidia-driver](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/091-nvidia-driver.jpeg)

So, install it:

```
sudo apt install nvidia-driver
```

After the reboot, your proprietary Nvidia driver should be activated. You can tweak it through the nvidia-settings:

![nvidia-settings](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/092-nvidia-settings.jpeg)

### AMD

AMD cards are more likely to work out of the box with the free  driver. If it is not the case, you can install a non-free driver. You  just need to install the non-free **AMD firmware** and some **Mesa** packages:

```
sudo apt install firmware-linux firmware-linux-nonfree libdrm-amdgpu1 xserver-xorg-video-amdgpu
```

Hopefully, you will get better AMD performance after this.

If you play games on your Debian machine, it is also worth enabling **Vulkan** support by installing these packages:

```
sudo apt install mesa-vulkan-drivers libvulkan1 vulkan-tools vulkan-utils vulkan-validationlayers
```

Finally, if you are going to use **OpenCL**, install all Mesa  OpenCL:

```
sudo apt install mesa-opencl-icd
```

Reboot your system and you should see the improvements in your AMD  graphics performance if it was not great before. To tweak your AMD  graphics, use the **AMD catalyst** settings:

![amd-catalyst](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/093-amd-catalyst.jpeg)

## 10. Install Restricted-extras packages

In Ubuntu, you can install all restricted packages such as **codes**, **Microsoft fonts** and **rar** archive support with the **ubuntu-restricted-extra** package. Unfortunately, this package does not exist in Debian. But you  still can install all these restricted-extras packages with this  command:

```
sudo apt install ttf-mscorefonts-installer rar unrar libavcodec-extra gstreamer1.0-libav gstreamer1.0-plugins-ugly gstreamer1.0-vaapi
```
You can also install singular fonts from the Synaptic package manager or similar. To install fonts that are similar to Calibri and Cambria, run 
```
sudo apt install fonts-crosextra-carlito fonts-crosextra-caladea
```

## 11. Install VLC

To be sure you will be able to play any video format, I recommend  installing VLC. It usually supports more video formats and it is also  more configurable and powerful than **GNOME Videos** app. For example, I showed in one of my previous posts how you can [use VLC to extract frames from a video](https://averagelinuxuser.com/video-to-images-with-vlc-media-player/).

![VLC in Debian 10](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/111-vlc-debian10.jpeg)

## 12. Install and configure Firewall

I have already talked about why I believe [it is better to have a firewall on Linux](https://averagelinuxuser.com/linux-firewall/). So, I won’t go into this discussion again. I usually recommend using **UFW** as it is the simplest firewall on Linux and yet it is sufficient for most users.

You can also try to run **GUFW**, which is a graphical interface of ufw and it makes all these operations easier. But it was very slow and was mostly **unresponsive on my Debian 10** installation. Let me know if it worked fine for you.

### Enable UFW

To enable UFW in Debian 10, you need to **install** it:

```
sudo apt install ufw
```

**Activate** it:

```
sudo ufw enable
```

And check its **status**:

```
sudo ufw status verbose
```

The default rules - to **deny incoming and allow outgoing** - works fine for most users.

### Edit UFW rules

If you need to open some ports, you can use the application name. To see the available names, run this command:

```
sudo ufw app list
```

And then open a port for the desired app. For example, to open an SSH port, run:

```
sudo ufw allow ssh
```

You can also use the port number (SSH port number is 22):

```
sudo ufw allow 22/tcp
```

If you want to delete some rules. Find out the rule number:

```
sudo ufw status numbered
```

And delete it:

```
sudo ufw delete 1
```

## 13. Install backup program

I was **surprised** to see that Debian doesn’t install any backup program by default. Even **rsync** is **not installed**:

![no rsync in Debian 10](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/131-rsync-debian10.jpeg)

Luckily, there are several **backup programs** you can choose from in the **Software Center**:

![backup programs in Software Center](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/132-backup-programs.jpeg)

I recommend installing **Grsync**. It is a simple and yet powerful graphical backup program. I reviewed **Grsync and LuckyBackUp** in my [Linux Backup with Graphical Programs](https://averagelinuxuser.com/linux-backup-with-gui/) post.
Another good program is **Timeshift**, quite simple and effective.

## 14. Configure Swappiness

**Swappiness** is 60 by default which is fine in most cases. But if you decrease this number, your system will use **RAM** more and start writing to **Swap** much later. Swap is an actual **disk space** and it is much **slower than RAM**. If you have 8G or more of RAM, you can force your system to use it at maximum.

First, check your swappiness value:

```
cat /proc/sys/vm/swappiness
```

Open the `/etc/sysctl.conf` file with Nano:

```
sudo nano /etc/sysctl.conf
```

And add `vm.swappiness = 10` at the very end of this file:

![Adding vm.swappiness to /etc/sysctl.conf](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/141-vm-swappiness.jpeg)

Press **Ctrl+O** to save the changed and **Ctrl+X** to exit Nano.

Reboot your system and you will see swappiness value of 10:

![Swappiness = 10](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/142-swappiness-10.jpeg)

I believe you will also see that your system doesn’t slow down until it almost completely fills the RAM.

## 15. Speed up the boot time

When you start Debian 10, you see this GRUB screen:

![GRUB screen in Debian 10](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/151-grub-screen-debian10.jpeg)

It may be useful if you multi-boot with other systems but if Debian  is the only system on your computer, you can disable this delay and  start booting Debian right away without waiting for 5 seconds.

To that end, you need to edit the GRUB configuration file:

```
sudo nano /etc/default/grub
```

And set GRUB_TIMEOUT to 0:

![Setting GRUB timeout to 0](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/152-grub-timeout-debian10.jpeg)

Then update GRUB:

```
sudo update-grub
```

Reboot and your Debian 10 will boot without the 5 seconds delay.

## 16. Enable Drive Cache

Another way to gain some **performance boost** for your  system is to enable Drive cache. Usually, the program waits until the  data is written to the disk before it goes to the next step. You can  disable this delay.

Open the **Disks** application → Select the drive **where you have your Debian 10** system installed  → Open the **Drive Settings** from the Disks menu → in the **Write cache tab**, enable Drive Cache.

![Enabling Drive Cache](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/161-drive-cache.jpeg)

You will also see the warning that there is a small risk of losing  data if your computer experiences a power outage. But it never happened  to me and I believe the performance improvement is worth taking the risk especially if you use a slow hard drive.

## 17. Xkill shortcut

This is a must-have on any system. When some application is not responding, you can **kill it** with the Ctrl+Alt+Esc shortcut. Pressing **Ctrl+Alt+Esc** will turn your **cursor** into a **cross** and when you click with it in on any application, it will be killed:

![Xkill in action](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/171-xkill-action.jpeg)

To enable this shortcut, open the **Settings → Devices → Keyboard → Add new shortcut**. Type `xkill` in the name and command and set *Ctrl+Alt+Esc* as a shortcut.

![setting xkill shortcut](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/172-xkill-shortcut.jpeg)

Now, whenever some **application misbehaves**, you can quickly kill it with *Ctrl+Alt+Esc*.

## 18. Ctrl+Alt+T to open Terminal

I also advise setting the **Ctrl+Alt+T** shortcut to open the **Terminal Emulator**. Similarly, to how you set xkill shortcut, add a new shortcut in **Settings → Devices → Keyboard → Add new shortcut**. Name it `Open Terminal` and type `gnome-terminal` as a command. To set the shortcut, press Ctrl+Alt+T.

![setting Terminal shortcut](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/181-terminal-shortcut.jpeg)

Now, accessing the terminal is only a matter of pressing these three keys - Ctrl+Alt+T.

## 19. Enable GNOME extensions

GNOME is a minimal and limited in functionality desktop. But it can  be improved and changed to an unrecognizable state with GNOME  extensions. So, let’s enable them.

You can access some GNOME Extensions through the **Software Center**. But it is better to configure direct installation from the **GNOME website** as there are more extensions.

To that end, make sure you have the `chrome-gnome-shell` package installed:

```
sudo apt install chrome-gnome-shell
```

Then, go to the [GNOME Extension website](https://extensions.gnome.org/) and you will be offered to install the **GNOME Shell integration add-on**. Do that.

![Installing GNOME Shell integration add-on](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/191-gnome-integration-add-on.jpeg)

After the installation, open the page of any GNOME Shell extension  and you will see the on/off button. You can use it to install the GNOME  Shell extensions right from the browser.

![Installing GNOME Shell extension](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/192-install-gnome-extension.jpeg)

There are many extensions you can try. Just do not make a Christmas tree from your system.

More over, it is useful to enable **tray icons**, an extension that brings to the top dock the icon symbol of active apps.

## 20. Install Desktop icons extension

Speaking about extensions, I believe many of you will find useful the [Desktop icons extension](https://extensions.gnome.org/extension/1465/desktop-icons/). By default, the Debian 10 desktop is empty and you cannot add any  icons, create folders or links here. After you install the Desktop icons extension, such functionality will be available.

![Desktop icons Shell extension is enabled](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/201-desktop-icons-extension.jpeg)

Install the [Desktop icons extension](https://extensions.gnome.org/extension/1465/desktop-icons/) from the [GNOME Extension website](https://extensions.gnome.org/).

## 21. Install additional themes

You won’t find many themes in Debian 10 by default but most likely  you would like to customize your desktop. To look, for example, like in  these screenshots:

![Customized GNOME desktop](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/211-customized-gnome.gif)

To install additional themes in Debian 10, you first need to fix the issue “Shell user-theme extension not enabled”:

![Shell user-theme extension not enabled](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/212-no-user-theme-extension.jpeg)

To fix it, installing the User Themes extension from the Software Center:

![Installing the User Theme extension](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/213-install-user-theme-extension.jpeg)

Now, you should be able to install custom themes in your Debian 10 system.

Search for the theme you like on [gnome-look.org](https://www.gnome-look.org/) and install it. For example, let’s install the [Orchis gtk theme](https://www.gnome-look.org/p/1357889/):

You need to **download it** from the **File tab**. Then **extract the archive** with the right mouse click and **move the extracted folder** to `.themes` folder in your home directory. You can use the **Ctrl+H** shortcut to see the hidden files that start with a dot in your home directory. If you do not have the `.themes` folder, create it.

![the .themes folder](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/214-themes-folder.jpeg)

Orchis theme is recommended to install with the [Tela icons theme](https://www.pling.com/p/1279924/). You can also **download it, extract and move the content** of the Tela folder to the `.icons` folder. Again, if you do not find the `.icons` folder  in your home directory, create it.

When your Orchis folder inside the `.themes` and Tela folders is in `.icons`. **Open Tweaks → Appearance → select Orchis** for applications and Shell and **Tela in icons**. You can also change the wallpaper and your Debian 10 desktop will look like this:

![Orchis themes and Tela in icons in Debian 10](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/215-orchis-tela-debian10.jpeg)

Following the same procedure, you can install most of the other themes from [gnome-look.org](https://www.gnome-look.org/). If you want to learn more about the options to get a custom GNOME theme, read my post on [Ubuntu GNOME customization](https://averagelinuxuser.com/customize-ubuntu-18-04/). Everything described in that post will work in Debian 10 GNOME too.

## 22. Add user image

This is trivial but let’s do it to complete the desktop theming. Go to **Account Settings** and set your user image. This will make your system more personal and more beautiful.

![Adding user image](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/221-user-image.jpeg)

## 23. Change LibreOffice look

To complete the theme settings, I also recommend adjusting the look of LibreOffice.

### Single Toolbar in LibreOffice

I believe a single toolbar has all essential tools of LibreOffice and you have more vertical space. This is especially helpful on small  screens.

To get the Single panel look, go to **View → Toolbar Layout → Single Toolbar**.

### LibreOffice Icons style

I also like a little more a non-default **icons style** in LibreOffice. To change the icons, open **Tools → Options → View → Style**. I like **Colibre** icons.

The final result looks like this:

![Colibre icons in LibreOffice](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/231-single-panel-colibre-libreoffice.jpeg)

## 24. Add Files bookmarks

In the **Files** file manager, you can quickly access  some of the folders from your home directory on the left panel. For  example, Documents, Downloads, Music, etc. But what if you want to add a custom bookmark there. You can do that!

Navigate to the folder you want to bookmark and **press Ctrl+D** or select to  **bookmark it from the folder menu**:

![bookmark folder in Files](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/241-bookmark-folder.jpeg)

Now, you can bookmark and quickly access your favorite folders from the left panel of File.

Unfortunately, I was not able to find how to reorder these custom  bookmarks to place them among the default bookmarks. If you know how to  do that, please let me know in the comments.

## 25. Enable Tray icons

Many of the third-party apps such as Dropbox, Skype, and others, do  not show up in the tray by default. I believe it is very convenient to  see those icons there. So, to enable such functionality, install [TopIcons Plus extension](https://extensions.gnome.org/extension/1031/topicons/)  from the [GNOME extensions webpage](https://extensions.gnome.org).

![Installing TopIcons Plus extension](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/251-topiconsplus-extension.jpeg)

You should see your tray icons appearing in the top panel. If you do not see them, try log out and log in.

![Tray icons in Debian 10](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/253-tray-icons.jpeg)

If the third-party icons are located in the center, you can open the settings of  TopIcons Plus extension in **Tweaks** and move them to the right.

![TopIcons Plus settings](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/252-topiconsplus-settings.jpeg)

## 26. Enable Night Light

Night Light will reduce the amount of blue light on your screen at  night time which is better for your eyes and sleep. This is a build-in  feature since [GNOME 3.24](https://www.gnome.org/news/2017/03/gnome-3-24-released/).

To activate it, go to **Settings → Devices → Display →** and set **Night Light** on.

![Enabling Night Light in Debian 10](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/261-night-light.jpeg)

## 27. Firefox settings

### Restore previous session

I like my Firefox to open the same tabs which I had open last time when I closed it. It saves a lot of time.

To enable this feature, open the **Firefox Preferences** and activate **Restore previous session**:

![Restore previous session option in Firefox](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/271-restore-session.jpeg)

To test it, open several tabs and close Firefox. When you open it again, it will start with the same tabs you had last time.

### DRM support

You can also enable **DRM support** in the settings. This will allow you to play DRM-controlled content such as **Netflix videos**, for example.

![DRM support in Firefox](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/272-drm-support.jpeg)

### Firefox Shell integration

If you followed the previous things to do after installing Debian 10, you should already have the GNOME Shell integration add-on installed in Firefox. If it is not the case, search for it in the Firefox add-ons  web-page and install it.

![GNOME Shell integration add-on](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/273-firefox-shell-integration.jpeg)

### Extend the Address/Search bar

I also prefer to remove the empty spaces from the sides of the Address/Search bar. To do that, **right-click** on the Firefox **top-panel** and select **Customize** and drag the empty spaces from the top-panel. This will extend the search/address bar of Firefox:

![Extend Firefox Address/Search bar](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/272-firefox-search-bar.jpeg)

## 28. Enable Snap and FlatPak Support

Snap and Flatpak programs are **distribution-agnostic**, so you can install any version of a Snap or Flatpak programs and **do not worry about** lack or conflict of **dependencies**.

To enable Snap and Flatpak support in Debian 10, open the **Software Center** and search for **Gnome  Software**. Select it and scroll down. You will find Snap and Flatpak in the addons bar. Enable either of them or both.

![Enable Snap and FlatPak Support in Debian 10](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/281-snap-flatpak.jpeg)

Restart the Software Center, and test Snap and Flatpak installation  by searching for a Snap or Flatpak program and installing it. You use  Kdenlive as an example. You will find two versions of Kdenlive and one  of them will be a Snap package. The information about the origin of a  program is provided in the package properties.

![Kdenlive Snap](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/282-kdenlive-Snap.jpeg)

## 29. Extend the battery life for Laptops

If you run Debian on a laptop, you can get some extra batter time by  installing the TLP power management tools. This tool provides the power  settings that are optimized for battery life.

```
sudo apt install tlp
```

After the installation reboot and you will find the TLP process active.

```
sudo systemctl status tlp
```

![TLP power management tool is active](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/291-tlp.jpeg)

## 30. Remove unnecessary apps

Luckily, Debian 10 GNOME doesn’t come with too much clutter. However, there are still some programs you may want to remove. There are two  ways to do that.

**First**, open the **Software Center** and in the **Installed tab**, you will see all major programs installed on your system . Remove those you are not going to use. I usually removed all the games.

![Software Center Installed tab](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/301-installed-software-tab.jpeg)

If you want to go even further, you can **open Synaptic**, go to the **Status section** and select to show only **Installed** programs. And start cleaning.

**Be careful** not to remove programs that are vital for your system. For example, I do not need a Bluetooth app, because I do  not have Bluetooth. But removing it will remove the whole GNOME desktop.

![Removing bluez](https://averagelinuxuser.com/assets/images/posts/2020-02-16-debian-10-after-install/302-bluez.jpeg)

So, you might **damage your system** by removing some programs from the Synaptic installed section, if you do not know what you are doing. Be careful!

## Your thought?

I hope you find these 30 things to do after installing Debian 10 useful. Let me know if you **would add something to this list**. The comments section is below. Thank you for reading.
